using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;

namespace Scripts
{
    public class Game : MonoBehaviour
    {
        [SerializeField] private Level _level;

        [SerializeField] private GameObject[] _prefabsPositions;
        [SerializeField] private GameObject[] _firstLevel;
        [SerializeField] private GameObject[] _secondLevel;
        [SerializeField] private GameObject[] _thirdLevel;
        [SerializeField] private GameObject _RestartButton;
        [SerializeField] private GameObject _loadPanel;
        [SerializeField] private GameObject _particle;

        [SerializeField] private Text _questionText;

        [SerializeField] private Button[] _button;

        private int answerId;
        private int _currentLevel;

        private void Start()
        {
            PanelFadeOut();
            _level.SetAnswer += Question;

            foreach (Button button in _button)
            {
                button.OnClicked += OnSelectAnswer;
            }
            Invoke(nameof(LoadLevel), 3f);
        }

        private void LoadLevel()
        {
            foreach (GameObject levelComponent in _firstLevel)
                levelComponent.SetActive(true);
            _level.FirstLevel();            
        }

        private void Question(int ansId)
        {
            _questionText.text = $"Find {_level.answersList[ansId]}";
            answerId = ansId;
            Invoke(nameof(QuestionSetActive), 1.5f);
        }

        private void QuestionSetActive()
        {
            _questionText.gameObject.SetActive(true);
            _questionText.DOFade(255, 2);
        }

        private void PanelFadeIn(GameObject Panel)
        {
            Image PanelColor = Panel.GetComponent<Image>();
            Panel.gameObject.SetActive(true);
            PanelColor.DOFade(255, 5);
            Invoke(nameof(Restsart), 5);
        }

        private void PanelFadeOut()
        {
            _loadPanel.gameObject.SetActive(true);
            Image PanelColor = _loadPanel.GetComponent<Image>();
            PanelColor.DOFade(0, 5);
            StartCoroutine(ClosePanelCoroutin(_loadPanel));
        }

        IEnumerator ClosePanelCoroutin(GameObject Panel)
        {
            yield return new WaitForSeconds(5);
            _loadPanel.gameObject.SetActive(false);
        }

        private void OnSelectAnswer(int i)
        {
            GameObject[] questions;
            questions = GameObject.FindGameObjectsWithTag("question");

            if (i == answerId)
            {
                _particle.GetComponent<ParticleSystem>().Play();
            }

            _prefabsPositions[i].transform.DOShakePosition(0.2f, 0.2f, 1, 1);

            StartCoroutine(DestroyCoroutine(i, questions));
        }

        private IEnumerator DestroyCoroutine(int i, GameObject[] questions)
        {
            yield return new WaitForSeconds(1.5f);

            foreach (GameObject question in questions)
                Destroy(question);

            if (i == answerId)
            {
                NextLevel();
            }
            else
                PanelFadeIn(_RestartButton);
        }

        public void Restsart()
        {
            SceneManager.LoadScene(0);
        }

        private void NextLevel()
        {
            _currentLevel++;

            if (_currentLevel == 1)
            {
                foreach (GameObject levelComponent in _secondLevel)
                    levelComponent.SetActive(true);
                _level.LoadLevel();
            }
            else if (_currentLevel == 2)
            {
                foreach (GameObject levelComponent in _thirdLevel)
                    levelComponent.SetActive(true);
                _level.LoadLevel();
            }
            else if (_currentLevel == 3)
            {
                _RestartButton.SetActive(true);
            }
        }

        private void OnDestroy()
        {
            _level.SetAnswer -= Question;

            foreach (Button button in _button)
            {
                button.OnClicked -= OnSelectAnswer;
            }
        }
    }
}

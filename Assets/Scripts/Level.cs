using System.Collections;
using UnityEngine;
using DG.Tweening;
using System;
using Random = UnityEngine.Random;

namespace Scripts
{
    public class Level : MonoBehaviour
    {
        [SerializeField] private ImageData[] _imageData;
        [SerializeField] private Button[] _buttons;

        [SerializeField] private float duration;
        [SerializeField] private float strength;
        [SerializeField] private int vibrato;
        [SerializeField] private float randomness;

        private ArrayList questionList = new ArrayList();
        private ArrayList numbersList = new ArrayList();
        private ArrayList lastAnswersList = new ArrayList();

        private int _currentLevel;
        private int spawnPoints;
        private bool isNumber;

        public ArrayList answersList = new ArrayList();

        public Action<int> SetAnswer;

        public void FirstLevel()
        {
            for (int i = 0; i < 3; i++)
            {
                BounceEffect(i);
            }

            Answer();
        }

        private void Answer()
        {
            int answerId = Random.Range(0, (answersList.Count - 1));
            var k = (string)answersList[answerId];

            lastAnswersList.Add(k);

            if (lastAnswersList.Contains(k) == true && answerId > 0)
            {
                answerId--;
            }
            else if (lastAnswersList.Contains(k) == true && answerId < 0)
            {
                answerId++;
            }

            SetAnswer?.Invoke(answerId);
            answersList.Clear();
            _currentLevel++;
        }

        public void LoadLevel()
        {
            questionList.Clear();
            numbersList.Clear();
            RandomVisualisation();

            if (_currentLevel == 1)
            {
                spawnPoints = 6;
            }
            else if (_currentLevel == 2)
            {
                spawnPoints = 9;
            }

            CreateList();

            for (int i = 0; i < spawnPoints; i++)
            {
                SpawnNumbers(i);
            }

            Answer();
        }

        private void RandomVisualisation()
        {
            int viz = Random.Range(0, 1);
            if (viz == 0)
                isNumber = true;
            else
                isNumber = false;
        }

        private void BounceEffect(int i)
        {
            CreateList();
            SpawnNumbers(i);

            _buttons[i].gameObject.SetActive(true);
            _buttons[i].transform.DOShakePosition(duration, strength, vibrato, randomness);
        }

        private void SpawnNumbers(int id)
        {

            int randomId = Random.Range(0, (questionList.Count - 1));
            var q = (string)questionList[randomId];
            var k = (int)numbersList[randomId];

            if (isNumber == true)
            {
                GameObject _number = Instantiate(_imageData[0]._imagePrefab[k], _buttons[id].transform.position, Quaternion.identity);
                _number.transform.parent = _buttons[id].transform;
            }
            else
            {
                GameObject _number = Instantiate(_imageData[1]._imagePrefab[k], _buttons[id].transform.position, Quaternion.identity);
                _number.transform.parent = _buttons[id].transform;
            }

            questionList.RemoveAt(randomId);
            numbersList.RemoveAt(randomId);

            answersList.Add(q);
        }

        private void CreateList()
        {
            if (isNumber == true)
            {
                for (int i = 0; i < _imageData[0]._imagePrefab.Length; i++)
                {
                    questionList.Add(_imageData[0]._description[i]);
                    numbersList.Add(i);
                }
            }
            else
            {
                for (int i = 0; i < _imageData[1]._imagePrefab.Length; i++)
                {
                    questionList.Add(_imageData[1]._description[i]);
                    numbersList.Add(i);
                }
            }
        }
    }
}

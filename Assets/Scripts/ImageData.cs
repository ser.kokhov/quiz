using UnityEditor;
using UnityEngine;

    [CreateAssetMenu(fileName = "ImagesData", menuName = "ScriptableObjects/ImageData", order = 1)]
    
    public class ImageData : ScriptableObject
    {
        [SerializeField] public GameObject[] _imagePrefab;
        [SerializeField] public string[] _description;
    }
